<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = []; 
        $stringItems = [['title'], 'string'];
        $integerItems  = ['statusId'];        
        if (\Yii::$app->user->can('updateActivity')) {
            $integerItems[] = 'categoryId';
        }
        $integerRule = [];
        $integerRule[] = $integerItems;
        $integerRule[] = 'integer';
        $rules[] = $stringItems;
        $rules[] = $integerRule;       
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
	public function getCategoryItem()
 	{
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }	
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'statusId']);
    }	
	
}
